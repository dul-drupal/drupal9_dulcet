// Matomo Tracking code for Serial Solutions

var _paq = window._paq = window._paq || [];
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
  var u="//analytics.lib.duke.edu/";
  _paq.push(['setTrackerUrl', u+'matomo.php']);
  _paq.push(['setSiteId', '48']);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
  g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
})();



// implementing the LibKey API from Third Iron on the EJP

var browzine = {
  api: "https://api.thirdiron.com/public/v1/libraries/229",
  apiKey: "eca52802-3ee5-4223-b869-8aee51d7712e",
  journalBrowZineWebLinkText: "View Journal Table of Contents",
};
 
browzine.script = document.createElement("script");
browzine.script.src = "https://s3.amazonaws.com/browzine-adapters/360-core/browzine-360-core-adapter.js";
document.head.appendChild(browzine.script);
