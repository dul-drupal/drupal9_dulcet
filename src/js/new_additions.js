/*

BLACKLIGHT CATALOG NEW ADDITIONS WIDGET
===========================
Prints a linked list of newly cataloged items matching a Blacklight query.

  Example #1) Show Thumbs:
  <h2>Lilly Current Literature (<span id="count-catalog-feed-items-lilly-currentlit"></span>)</h2>
  <ul id="catalog-feed-items-lilly-currentlit" data-query="f%5Blocation_hierarchy_f%5D%5B%5D=duke%3Adukelily%3Adukelilycure" data-showcount="true" data-showthumbs="true">
    // repeat for number of placeholders you would like to be present in a single row
    <li class="placeholder">
      <span class="sr-only">loading...</span>
      <div class="new-additions-thumb-skeleton"></div>
      <div class="new-additions-title-skeleton"></div>
      <div class="new-additions-author-skeleton"></div>
    </li>
  </ul>

  Example #2) No Thumbs, Prefer Finding Aid Link:
  <ul id="catalog-feed-items-hartman-mss" class="list-unstyled" data-query="f%5Bauthor_facet_f%5D%5B%5D=John+W.+Hartman+Center+for+Sales%2C+Advertising+%26+Marketing+History&f%5Bresource_type_f%5D%5B%5D=Archival+and+manuscript+material" data-fetchresults="4" data-preferfaid="true" data-showcount="true" data-showthumbs="false">
    <li><span class="sr-only">loading</span>...</li>
  </ul>

  Options (* = optional)
  -------
  id: catalog-feed-items; can have multiple on page by appending -2, -3, -dvds, etc.
  data-query: Blacklight query parameters & values
  data-showcount*: render the count of matching results in an element w/id 'count-{id}'
  data-preferfaid*: if 'true', links to RL finding aid if exists, rather than catalog item page
  data-fetchresults*: how many search results to retrieve? (default: 20)
  data-showitems*: how many items to display? the rest will be display: hidden; (default: all).
      Useful esp. for items w/unknown number of legitimate thumbs -- may have to fetch 30
      to find 10 items w/legit thumbs; of those 10, how many to display?
  data-showthumbs*: if 'true', will render a thumbnail image from Syndetics
  data-singlerow*: if 'true', will present using flexbox; number of data-showitems will display
      all on one row, evenly distributed. (default: 'false')

*/

jQuery(document).ready(function ($) {

  function shorten(text, maxLength) {
    var ret = text;
    if (ret.length > maxLength) {
        ret = ret.substr(0,maxLength-3) + "...";
    }
    return ret;
  }

  // hack to decode html entities
  function decodeEntities(string) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = string;
    return textArea.value;
  }

  function is_nested_json(string) {
    if (string.startsWith('{')) { 
     return true;
    } else {
     return false;
    }
  }

  // deal with nested json
  function clean_stringified_json(string) {
    var cleaned_string = string.replace(/&quot;/g, '\"');
    if (cleaned_string.indexOf('}<br />{') >= 0 ) {
      cleaned_string = cleaned_string.split('<br />')[0]
    }
    return cleaned_string;
  }

  function parse_nested_json(string) {
    if (is_nested_json(string) == true) {
     return JSON.parse(clean_stringified_json(string));
    }
  }

  function find_faid(obj) {
    if(obj.attributes.hasOwnProperty('url_a')) {
      parsed_faid = parse_nested_json(obj.attributes.url_a.attributes.value);
      if (parsed_faid.type == 'findingaid') {
        return parsed_faid.href;
      }
    }
    else {
      return "";
    }
  }

  function find_author(obj) {
    if(obj.attributes.hasOwnProperty('statement_of_responsibility_a')) {
      return shorten(obj.attributes.statement_of_responsibility_a.attributes.value,40);
    }
    else {
      return "";
    }
  }

  function build_thumb(obj) {
    thumb_url = 'https://www.syndetics.com/index.aspx?client=trlnet';
    
    if(obj.attributes.hasOwnProperty('isbn_number_a')) {
      thumb_url += '&isbn=';
      thumb_url += obj.attributes.isbn_number_a.attributes.value;
    }
    if(obj.attributes.hasOwnProperty('upc_a')) {
      thumb_url += '&upc=';
      thumb_url += obj.attributes.upc_a.attributes.value.replace('UPC: ','');
    }
    if(obj.attributes.hasOwnProperty('oclc_number')) {
      thumb_url += '&oclc=';
      thumb_url += obj.attributes.oclc_number.attributes.value;
    }
    thumb_url += '/LC.GIF';
    return thumb_url;
  }

  function get_title(obj) {
    if(obj.attributes.hasOwnProperty('title_main')) {
      // decode html entities
      $decoded_title = decodeEntities(obj.attributes.title_main.attributes.value)
      return shorten($decoded_title,80);
    }
    else {
      return "[Untitled]";
    }
  }

  function find_description(obj) {
    if(obj.attributes.hasOwnProperty('note_summary_a')) {
      // decode html entities
      $decoded_description = decodeEntities(obj.attributes.note_summary_a.attributes.value)
      // remove quotes in note
      return shorten($decoded_description,100).replace(/"/g, "");
    }
    else {
      return "";
    }
  }

  var catalog_base_url = 'https://find.library.duke.edu';

  $("[id^=catalog-feed-items]").each(function(){
    var $this_widget = $(this);
    var widget_id = $this_widget.attr('id');
    var blacklight_query = $this_widget.attr('data-query');
    var fetchresults = parseInt($this_widget.attr('data-fetchresults')) || 20;
    var showitems = parseInt($this_widget.attr('data-showitems')) || 10;
    var showcount = $this_widget.attr('data-showcount') || 'false';
    var prefer_faid = $this_widget.attr('data-preferfaid') || 'false';
    var showthumbs = $this_widget.attr('data-showthumbs') || 'true';
    var singlerow = $this_widget.attr('data-singlerow') || 'false';

    if (showthumbs == 'true') {
      $this_widget.addClass('new-additions-with-thumb');
    } else {
      $this_widget.addClass('new-additions-text-only');
    }

    if (singlerow == 'true') {
      $this_widget.addClass('single-row');
    }

    var sort_terms = 'date_cataloged+desc%2C+publication_year_isort+desc';
    $formatted_query = catalog_base_url + '/catalog.json?' + blacklight_query  + '&amp;per_page=' + fetchresults + '&amp;sort=' + sort_terms;

    $.ajax({
      url: $formatted_query,
      type: "GET",
      dataType: 'json',
      success: function (results) {
        
        // remove placeholder list items
        if (showthumbs == 'true') {
          $("#" + widget_id).empty();
        }

        $(this).html('');

        // Get total result count for query
        var total_results = results.meta.pages.total_count.toString();

        // Prettify with commas
        var result_count = total_results.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

        // Add the result count to an element on the page
        $('#count-'+widget_id).html(result_count);

        $.each(results.data, function(idx, obj) {
          var item = {};

          item = {
            id:           obj.id,
            title:        get_title(obj),
            link:         catalog_base_url + '/catalog/' + obj.attributes.local_id.attributes.value,
            author:       find_author(obj),
            findingaid:   find_faid(obj),
            description:  find_description(obj),
            thumb:        build_thumb(obj),
          }

          if (prefer_faid == 'true' && item.findingaid) {
            item.link = item.findingaid;
          }

          var $item_li = $('<li />');
          var $item_link_markup = $('<a />').attr('href', item.link).attr('title', item.description).attr('class', 'new-additions-link');
          var item_title_markup = '<div class="new-additions-title">' + item.title + '</div>';
          var item_info_markup = '<div class="muted small new-additions-author">' + item.author + '</div>';

          if (showthumbs == 'true') {
            // First check that the thumbnail is actually present / legitimate
            var $thumb_img = $("<img />").attr('src', item.thumb)
                .on('load', function() {
                    if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth < 0) {
                      $item_li.attr('class','badthumb');
                      $item_li.attr('style','display: none');
                      $item_li.remove();
                    } else {
		                  $thumb_img.attr('alt', '');
                      $item_link_markup.prepend('<br/>');
                      $item_link_markup.prepend($thumb_img);
                    }
                })
                // if there's no thumbnail, remove the item
                .on('error', function() {
                  $item_li.attr('class','badthumb');
                  $item_li.attr('style','display: none');
                  $item_li.remove();
                });
          }

          $item_link_markup.append(item_title_markup);
          $item_li.append($item_link_markup);
          $item_li.append(item_info_markup);
          $this_widget.append($item_li);

        }); // end document iterator

      } // end success
    }); // end .ajax({})

  }); // end #catalog-feed-items* iterator

});
