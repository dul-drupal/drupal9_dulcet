/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your application. See https://github.com/JeffreyWay/laravel-mix.
 |
 */
const proxy = 'http://0.0.0.0';
const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Configuration
 |--------------------------------------------------------------------------
 */
mix
  .webpackConfig({
      // Use the jQuery that ships with Drupal to avoid conflicts
      // see https://www.drupal.org/project/radix/issues/3067615
      externals: {
        jquery: 'jQuery'
      }
    })
  .setPublicPath('assets')
  .disableNotifications()
  .options({
    processCssUrls: false,
    fileLoaderDirs: {
      fonts: 'assets/webfonts'
    }
  });

/*
 |--------------------------------------------------------------------------
 | Browsersync
 |--------------------------------------------------------------------------
 */
mix.browserSync({
  proxy: proxy,
  files: ['assets/js/**/*.js', 'assets/css/**/*.css'],
  stream: true,
});

/*
 |--------------------------------------------------------------------------
 | SASS
 |--------------------------------------------------------------------------
 */
mix.sass('src/sass/drupal9_dulcet.style.scss', 'css');

/*
 |--------------------------------------------------------------------------
 | JS
 |--------------------------------------------------------------------------
 */
mix.js('src/js/drupal9_dulcet.script.js', 'js');

/*
 |--------------------------------------------------------------------------
 | FontAwesome fonts
 |--------------------------------------------------------------------------
 */

mix.copyDirectory(
  'node_modules/@fortawesome/fontawesome-free/webfonts',
  'assets/webfonts'
);
