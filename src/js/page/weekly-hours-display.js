// PREREQUISITES:
// There needs to be a basic page created with path /about/hours
// -- content is as follows:
//
// <div id="weekly-nav"><button class="btn btn-primary" id="prevBtn" type="button">« Prev</button><button class="btn btn-primary" id="nextBtn" type="button">Next »</button></div>
//
// <div id="dul-weekly-hours">loading...</div>
// -- note that 'loading' text could be replaced with a spinner graphic, etc
//
// <hr />
//
// <section aria-labelledby="professional-hours">
// 	<h2 id="professional-hours">Duke University Professional School Libraries Hours</h2>
// 	<ul>
// 		<li><a href="https://library.divinity.duke.edu/hours">Divinity School Library</a></li>
// 		<li><a href="https://library.fuqua.duke.edu/hours.htm">Ford Library, Fuqua School of Business</a></li>
// 		<li><a href="https://law.duke.edu/lib/#hours">Goodson Law Library</a></li>
// 		<li><a href="https://mclibrary.duke.edu/accounts-access/building-access">Medical Center Library</a></li>
// 	</ul>
// </section>


window.onload = function() {
  
  // make sure the target div exists!
  if (document.getElementById('dul-weekly-hours')) {

    // make the API request
    const xhr = new XMLHttpRequest()
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        // console.log(typeof xhr.responseText)
        // console.log(xhr.responseText)

        const jsonData = JSON.parse(xhr.responseText);

        // start on current week
        var currentWeek = 0;

        // this is the output for the placeholder div
        var hoursContent = "";

        // disable prev button by default
        document.getElementById('prevBtn').classList.add("disabled");

        // prev / next buttons
        document.getElementById('prevBtn').addEventListener('click', function() {
          document.getElementById('nextBtn').classList.remove("disabled");
          if (currentWeek == 0) {
            currentWeek = 0
          } else {
            currentWeek -= 1
          }
          updateContent(currentWeek);
        });

        document.getElementById('nextBtn').addEventListener('click', function() {
          document.getElementById('prevBtn').classList.remove("disabled");
          if (currentWeek == 15) {
            currentWeek = 15
          } else {
            currentWeek += 1
          }
          updateContent(currentWeek);
        });

        // get the initial hours content - starting with 0
        updateContent(currentWeek);

        // where the sausage gets made
        function updateContent(num) {
          
          hoursContent = "";
          current = num;
          daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
          monthsOfYear = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
          locationsArray = [11088, 17335, 13162, 13161, 11063, 23559, 11150, 14354];
          // perkins: 11088
          // study spaces: 17335
          // general public: 13162
          // rubenstein: 13161
          // Lilly: 11063
          // Lilly at Bishop’s House: 23559  -- will need to remove after renovation is complete
          // lilly public: 17439 -- removed for renovation, will need to update with new calendar once reopened!
          // music: 11150
          // exhibits: 14354


          // update button states
          if (current == 0) {
            document.getElementById('nextBtn').classList.remove("disabled");
            document.getElementById('prevBtn').classList.add("disabled");
          }
          if (current == 11) {
            document.getElementById('nextBtn').classList.add("disabled");
            document.getElementById('prevBtn').classList.remove("disabled");
          }


          // start assembling the table html
          hoursContent += '<div class="table-responsive"><table id="dul-hours-table" class="table table table-striped table-bordered sticky-table">';
          hoursContent += '<thead><tr><th scope="col" class="sticky"></th>';

          function calDays(i,y) {
            let found = jsonData.locations[i].weeks[y];
            return found;
          }


          // format header row dates
          function formateDate(date) {
            var dateSeg = date.split('-');
            var newDate = new Date(dateSeg[0], dateSeg[1] - 1, dateSeg[2]); 
            return daysOfWeek[newDate.getDay()] + ', ' + monthsOfYear[newDate.getMonth()] + ' ' + newDate.getDate();
          }


          // get today's date for setting active state
          var today = new Date();
          var formattedToday = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
          var activeDateRow = '';


          // render the header row with dates
          daysOfWeek.forEach( renderHeader );
          function renderHeader(day) {
            hoursContent += '<th scope="col" class="sticky">' + formateDate(calDays(0,current)[day].date) + '</th>';
            
            // check if it's an active day
            if ( formateDate(formattedToday) == formateDate(calDays(0,current)[day].date) ) {
              activeDateRow = calDays(0,current)[day].date;
            }

          }

          hoursContent += '</tr></thead><tbody>';

          
          // get the location by ID
          function libLocation(id) {
            let found = jsonData.locations.find(item => item.lid == id);
            if (found === undefined) {
              return null;
            }
            return found;
          }


          // loop through locations
          locationsArray.forEach( renderLocation );
          
          function renderLocation(id) {
            if (libLocation(id) === null) {
              // Skip this iteration as the location was not found in the API result
              return;
            }
            hoursContent += '<tr><th><a href="' + libLocation(id).url + '" title="View hours by month">' + libLocation(id).name + '</a></th>';
            i = 0;
            while (i < 7) {
              renderWeek(id,i);
              i ++;
            }
            hoursContent += '</tr>';
          }

          function renderWeek(id,i) {
            active = '';
            if (libLocation(id).weeks[current][daysOfWeek[i]].date == activeDateRow) {
              active = 'today';
            }

            renderedContent = libLocation(id).weeks[current][daysOfWeek[i]].rendered;

            if (renderedContent == '24 Hours') {
              renderedContent = 'Open 24 Hours';
            }

            hoursContent += '<td class="' + active + '">' + renderedContent + '</td>'
          }

          hoursContent += '</tbody></table></div>';


          // update the content of the placeholder div
          var hoursDiv = document.querySelector('#dul-weekly-hours');
          hoursDiv.innerHTML = hoursContent;

        }

      }
    };
    xhr.open('GET', 'https://duke.libcal.com/widget/hours/grid?iid=971&format=json&weeks=16', true);
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.send({ 'request': "authentication token" });
    //xhr.send(null);

  }
}
